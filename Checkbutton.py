from tkinter import * #Vi aktiverer tkinter
window = Tk() #Vi laver et tikinter vindue
window.title("Det store Valg") #Vi giver vinduet en titel
window.geometry('350x200') #Vi giver vinduet en størrelse

lbl1 = Label(window, text = "label 1") #vi laver et nyt label med teksten "label 1"
lbl1.grid(column=0, row=2) #vi sætter label 1s position ved hjælp a grids

lbl2 = Label(window, text = "label 2") #vi laver endu et label denne gang med teksten "label 2"
lbl2.grid(column=0, row=3) #vi sætter label 2s position igen ved hjælp a grids

def click(): #vi definere en funktion som skal kaldes af vores knap

    if chk1_state.get() == 1: #Vi ser på om chk1_state, som er den variable som er linket til chk1's værdi, er lig med 1. Hvis den er ændre vi lbl1 ellers sætter vi den til start værdien
        lbl1.configure(text="du har valgt valg 1")
    else:
        lbl1.configure(text="label 1")

    if chk2_state.get() == 1: #her gør vi det samme som i det forrige if statement, men i forhold til knap 2
        lbl2.configure(text="du har valgt valg 2")
    else:
        lbl2.configure(text="label 2")

#Vi definere vores indsend knap og linker den til vores click funktion
btn = Button(window, text="Indsend", command = click)
btn.grid(column=2, row=0, padx=200)

chk1_state = IntVar() #Vi definerer den variable som vi bruger til at holde styr på checkbutton1's værdi
chk1 = Checkbutton(window, text="valg 1",variable=chk1_state) #Her definere vi vores første checkbutton og sætter dens position med grids
chk1.grid(column=0, row=0)

chk2_state = IntVar() #Vi definere den variable som skal holde styr på vores anden checkbuttons værdi
chk2 = Checkbutton(window, text="valg 2",var=chk2_state) #Her definere vi vores anden checkbutton og sætter dens position med grids
chk2.grid(column=0, row=1)

window.mainloop() #Tkinters mainloop funktion køres på vores vindue