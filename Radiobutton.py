from tkinter import *

window = Tk()
window.title("Den Vilde Radio")
window.geometry('350x200')
#Vores vindue difineres, navngives og størrelsen angives

lbl1 = Label(window, text = "label 1")
lbl1.grid(column=0, row=90)

lbl2 = Label(window, text = "label 2")
lbl2.grid(column=0, row=91)
#De to labels difineres

var1 = IntVar()
var2 = IntVar()
var1.set(0)
var2.set(0)
#Her difineres de variable der bruges til at angive knappernes position, der sættes derefter til 0 posistionen.

m1 = PanedWindow(window, orient=HORIZONTAL)
m1.configure(bg="green")
m1.grid(column=0, row=0, padx = (0,20))

m2 = PanedWindow(window, orient=HORIZONTAL)
m2.configure(bg="green")
m2.grid(column=1, row=0)
#Her difineres de to mindre vinduer som knapperne skal side i

rad1 = Radiobutton(m1, text = "rad 1", variable = var1, value = 0)
rad1.grid(column=0, row=0)
rad2 = Radiobutton(m1, text = "rad 2", variable = var1, value = 1)
rad2.grid(column=0, row=1)

rad3 = Radiobutton(m2, text = "rad 3", variable = var2, value = 0)
rad3.grid(column=0, row=2)
rad4 = Radiobutton(m2, text = "rad 4", variable = var2, value = 1)
rad4.grid(column=0, row=3)
#Her difineres knapperne og sættes ind i de to mindre vinduer

def click(): #Denne funktion køre når indsend knappen trykkes
    if var1.get() == 1:
        lbl1.configure(text="du har valgt rad 2")
    else:
        lbl1.configure(text="du har valgt rad 1")
    if var2.get() == 1:
        lbl2.configure(text="du har valgt rad 4")
    else:
        lbl2.configure(text="du har valgt rad 3")
    #Knappernes position aflæsses og skrives i labelsene


valg = Button(window, text="indsend", command=click)
valg.grid(column=3, row=0)
#Indsend knappen difineres
window.mainloop() #Tkinters mainloop funktion køres på vores vindue